"use strict"

const stylus = require('stylus')
const fs = require('fs')

module.exports.render = (buffer, opts = {}) => new Promise((res, rej) => {
  let style = stylus(buffer)

  for (let [key, value] of Object.entries(opts))
    style.set(key, value)
  
  style.render((err, css) => err ? rej(err) : res(css))
})

module.exports.renderFile = (file, opts = {}) => new Promise((res, rej) => {
  fs.readFile(file, (err, buffer) => {
    if (err)
      return rej(err)

    buffer = buffer.toString()

    module.exports.render(buffer, opts).then(res);
  })
})